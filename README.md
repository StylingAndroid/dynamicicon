#   Dynamic Icon

This is sample code which corresponds to a series on the technical blog [Styling Android](http://blog.stylingandroid.com/archives/1471) which describes how to create an image whereby areas of colour can be changed programatically.

This code is released under the Apache 2.0 license.