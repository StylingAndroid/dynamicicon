package com.stylingandroid.dynamicicon;

import android.app.Activity;
import android.graphics.*;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.SeekBar;

public class DynamicIconActivity extends Activity
{
    private float red = 1.0f;
    private float green = 1.0f;
    private float blue = 1.0f;
    private ImageView image = null;
    private Bitmap background = null;
    private Bitmap foreground = null;
    private Paint paint = new Paint();

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.main );
        background = BitmapFactory.decodeResource( getResources(), R.drawable.ic_menu_play_clip );
        foreground = BitmapFactory.decodeResource( getResources(), R.drawable.ic_menu_play_clip_mask );
        SeekBar redSeek = (SeekBar) findViewById( R.id.redSeek );
        SeekBar greenSeek = (SeekBar) findViewById( R.id.greenSeek );
        SeekBar blueSeek = (SeekBar) findViewById( R.id.blueSeek );
        image = (ImageView) findViewById( R.id.image );

        image.setBackgroundColor( Color.WHITE );
        image.setImageResource( R.drawable.ic_menu_play_clip );

        redSeek.setMax( 0xFF );
        redSeek.setProgress( 0xFF );
        redSeek.setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener()
        {

            @Override
            public void onStopTrackingTouch( SeekBar arg0 )
            {
            }

            @Override
            public void onStartTrackingTouch( SeekBar arg0 )
            {
            }

            @Override
            public void onProgressChanged( SeekBar arg0, int pos, boolean arg2 )
            {
                red = (float) pos / (float) 0xFF;
                updateImage();
            }
        } );
        greenSeek.setMax( 0xFF );
        greenSeek.setProgress( 0xFF );
        greenSeek.setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener()
        {

            @Override
            public void onStopTrackingTouch( SeekBar arg0 )
            {
            }

            @Override
            public void onStartTrackingTouch( SeekBar arg0 )
            {
            }

            @Override
            public void onProgressChanged( SeekBar arg0, int pos, boolean arg2 )
            {
                green = (float) pos / (float) 0xFF;
                updateImage();
            }
        } );
        blueSeek.setMax( 0xFF );
        blueSeek.setProgress( 0xFF );
        blueSeek.setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener()
        {

            @Override
            public void onStopTrackingTouch( SeekBar arg0 )
            {
            }

            @Override
            public void onStartTrackingTouch( SeekBar arg0 )
            {
            }

            @Override
            public void onProgressChanged( SeekBar arg0, int pos, boolean arg2 )
            {
                blue = (float) pos / (float) 0xFF;
                updateImage();
            }
        } );
        updateImage();
    }

    private void updateImage()
    {
        if ( foreground != null && background != null )
        {
            float[] transform =
                    { red, 0, 0, 0, 0, 0, green, 0, 0, 0, 0, 0, blue, 0, 0, 0, 0, 0, 1.0f, 0 };
            ColorFilter cf = new ColorMatrixColorFilter( transform );
            Bitmap bm = combineImages( background, foreground, cf );
            image.setImageBitmap( bm );
        }
    }

    public Bitmap combineImages( Bitmap bgd, Bitmap fg, ColorFilter filter )
    {
        Bitmap bmp;

        int width = bgd.getWidth() > fg.getWidth() ? bgd.getWidth() : fg.getWidth();
        int height = bgd.getHeight() > fg.getHeight() ? bgd.getHeight() : fg.getHeight();

        bmp = Bitmap.createBitmap( width, height, Bitmap.Config.ARGB_8888 );

        Canvas canvas = new Canvas( bmp );
        canvas.drawBitmap( bgd, 0, 0, null );
        paint.setColorFilter( filter );
        canvas.drawBitmap( fg, 0, 0, paint );

        return bmp;
    }
}
